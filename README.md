# **Project in Software Engineering - 2020S - JKU, LINZ**

Project Title: “Monitoring of Software Development Processes”

Goal
====
Visualize progress of flexible software development processes.

Task
====
- Read process information and events from existing process engine (java)
- Implement a prototype for displaying structure of process and progress of individual tasks (java
or javascript)

- Model example simple design process and replay events from existing real world data set (java)

Content
========
- 1.Design document
- 2.Requirements and development document
- 3.Presentation slide
- 4.Video-HowTo
- 5.Monitor Component - Postman file and documentation
- https://www.getpostman.com/collections/c304f826a7d1b41776dc

Sources:
========
- Monitor Component and Reminder Component 
- https://gitlab.com/kumar.neelamegam17/github-webhook.git  

Contact / Support
=================
- Muthukumar Neelamegam
- kumar.neelamegam17@gmail.com | k11834456@students.jku.at


