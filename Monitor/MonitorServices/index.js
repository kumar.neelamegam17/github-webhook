const express = require('express');
const bodyParser = require('body-parser');
const basicAuth = require('./_helpers/basic-auth');
const errorHandler = require('./_helpers/error-handler');
const cors = require('cors');

//***************************************************************************
//Initialization
// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

app.use(cors());

// use basic HTTP auth to secure the api
//app.use(basicAuth);

// global error handler
app.use(errorHandler);

//***************************************************************************
// Configuring the database
const dbConfig = require('./app/config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

//***************************************************************************
// define a home route
app.get('/', (req, res) => {
    res.json({"message": "Welcome to software project monitoring application. Its Active"});
});

//***************************************************************************
/*// Require Events routes
app.get('/view', function(req,res){
    res.sendfile(__dirname + '/Reminder/viewall.html');
});*/


require('./app/routes/event.routes.js')(app);
// listen for requests
app.listen(5000, () => {
    console.log("Server is listening on port 5000");
});
//***************************************************************************
