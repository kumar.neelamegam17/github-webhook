module.exports = {
    url: 'mongodb://localhost:27017/sms_monitor?keepAlive=true&poolSize=30&autoReconnect=true&socketTimeoutMS=360000&connectTimeoutMS=360000',
    useUnifiedTopology: true
}