module.exports = (app) => {
    const events = require('../controllers/event.controller.js');
    //Create an events from github web hook
    app.post('/addEvent', events.create);//add all the events to the cache (Mongodb)
 /*   app.get('/getEvents', events.findAll);  // Retrieve all Events
    app.get('/getEvent/:eventId', events.findOne);    // Retrieve a single Event with eventId
    app.put('/setEvent/:eventId', events.update);    // Update a Event with eventId
    app.delete('/removeEvent/:eventId', events.delete);    // Delete a Event with eventId
*/
    //webhookurl
    app.get('/startRemoteHookURL/', events.startRemoteHookURL);
    app.get('/stopRemoteHookURL/', events.stopRemoteHookURL);

    //setting up webhook
    app.post('/createWebHook/', events.createWebHook);

    //cached objects manipulations
    //returns latest object state
    app.get('/getObjectById/:Id', events.getObjectById); // --> done

    //returns all previous events for that object
    app.get('/getObjectHistoryById/:Id', events.getObjectHistoryById);

    //provides via server sent events (SSE) any new change events limited to the ids mentioned above
    app.get('/monitorObjectById/:Id', events.monitorObjectById);

    //provides all new issue updates, including new issues, deleted issues, for the identified repository)
    app.get('/monitorIssues/:repoId', events.monitorIssues);

    //provide events on all new commits,  (i.e., new commits and commenting thereof)
    app.get('/monitorCommits/:repoId', events.monitorCommits);

    //provide events on all new pull requests (i.e., new pull requests and changes thereof)
    app.get('/monitorPullRequests/:repoId', events.monitorPullRequests);

    //provide events on all new pull requests  (i.e., new pull requests and changes thereof)
    app.get('/monitorPushWithStatus/:repoId', events.monitorPushWithStatus);

    app.get('/monitorPush/:repoId', events.monitorPush);

    //a simple passthrough of all events received
    app.get('/monitorAll', events.monitorAll);

    //GitHub direct access
    //get specific issue details directly from GitHub
    app.get('/getIssuefromGitHub/:owner/:repo/:issue_number', events.getIssuefromGitHub);

    app.get('/getRepositoryInfo/', events.getRepositoryInfo);

    app.post('/addCommitComment', events.addCommitComment);

    app.get('/getAssignedIssues', events.getAssignedIssues);

}