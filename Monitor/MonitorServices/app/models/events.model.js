const mongoose = require('mongoose');

const EventSchema = mongoose.Schema({
    _id: String,
    response_data: String,
    event_type: String,
    date: String,
    active: Boolean,
}, {
    timestamps: true
});

module.exports = mongoose.model('events', EventSchema);