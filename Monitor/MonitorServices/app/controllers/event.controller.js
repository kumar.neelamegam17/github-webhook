const Event = require('../models/events.model.js');
const repositorInfo = require('../config/repository.config');
var Promise =require('promise');
const { Octokit } = require("@octokit/rest");
const octokit = new Octokit();
const ngrok = require('ngrok');
var dateFormat = require('dateformat');
/*
//!***************************************************************************
// Retrieve and return all events from the database.
exports.findAll = (req, res) => {
    Event.find()
        .then(events => {
            res.send(events);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    });
};
//!***************************************************************************
// Find a single event with a eventId
exports.findOne = (req, res) => {
    Event.findById(req.params.eventId)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "Event not found with id " + req.params.eventId
                });
            }
            res.send(event);
        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Event not found with id " + req.params.eventId
            });
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.eventId
        });
    });
};
//!***************************************************************************
// Update a event identified by the eventId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body) {
        return res.status(400).send({
            message: "Event content can not be empty"
        });
    }

    // Find event and update it with the request body
    Event.findByIdAndUpdate(req.params.eventId, {
        response_data: req.body.response_data ||  "No data",
        date: req.body.date,
        active: req.body.active
    }, {new: true})
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "Event not found with id " + req.params.eventId
                });
            }
            res.send(event);
        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Event not found with id " + req.params.eventId
            });
        }
        return res.status(500).send({
            message: "Error updating event with id " + req.params.eventId
        });
    });
};
//!***************************************************************************
// Delete a event with the specified eventId in the request
exports.delete = (req, res) => {
    Event.findByIdAndRemove(req.params.eventId)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "Event not found with id " + req.params.eventId
                });
            }
            res.send({message: "Event deleted successfully!"});
        }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Event not found with id " + req.params.eventId
            });
        }
        return res.status(500).send({
            message: "Could not delete event with id " + req.params.eventId
        });
    });
};
*/


//***************************************************************************
exports.startRemoteHookURL = (req, res) => {
    (async function() {
        const url = await ngrok.connect({
            proto: 'http', // http|tcp|tls, defaults to http
            addr: 5000,
        });
        myObj = new Object()
        myObj["url"] = url; myObj["status"] = "started";
        res.send(myObj);
    })();
};
//***************************************************************************
exports.stopRemoteHookURL = (req, res) => {
    (async function() {
        await ngrok.disconnect(); // stops all
        await ngrok.kill(); // kills ngrok process
        const apiUrl = ngrok.getUrl();
        myObj = new Object()
        myObj["url"] = apiUrl; myObj["status"] = "stopped";
        res.send(myObj);
    })();
};
//***************************************************************************
// Create and Save a new Event
exports.create = (req, res) => {
    // Validate request
    if(!req.body) {
        return res.status(400).send({
            message: "Event content can not be empty"
        });
    }
    var now = new Date();
    dateFormat.masks.hammerTime = "yyyy-mm-dd HH:MM:ss";
    // Create a Event
    const event = new Event({
        _id:req.headers['x-github-delivery'],
        response_data: JSON.stringify(req.body) || "No data",
        event_type: req.headers['x-github-event'],
        date: dateFormat(now, "hammerTime"),
        active: true
    });

    //console.log(dateFormat(now, "hammerTime"));

    // Save Event in the database
    event.save()
        .then(data => {
            res.send("Success");
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the events."
        });
    });
};
//***************************************************************************
/**
 * Events manipulations
 * issues, commits, pull-requests
 */
//***************************************************************************
// 1. returns latest object state
/*
Get one event information by passing objectId
 */
exports.getObjectById = (req, res) => {
    Event.findById(req.params.Id)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "getObjectById not found with id " + req.params.Id
                });
            }
            res.send(event);
        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "getObjectById not found with id " + req.params.Id
            });
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.Id
        });
    });
};
//***************************************************************************
// 2. returns all previous events for that object
/*
This method is used to filter history of events from selected event
Example:
There are multiple pull_request are available, so using one objectId then identify the eventType and eventId and get all the corresponding
event history.
objectId-> get eventType, eventId -> filter all events
 */
exports.getObjectHistoryById = (req, res) => {
    Event.findById(req.params.Id)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "getObjectHistoryById not found with id " + req.params.Id
                });
            }

            var eventType = event.event_type;
            var eventId = JSON.parse(event.response_data)[eventType].id;
            //console.log("EventId:"+eventId);
            searchEventById(eventId, eventType, res);

        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "getObjectHistoryById not found with id " + req.params.Id
            });
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.Id
        });
    });
};

function searchEventById(eventId, eventType, res)
{
    var query={event_type: eventType};
    Event.find(query)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "Error retrieving event with id " + eventId +"/"+ eventType
                });
            }
            var i;
            var iLength = event.length;
            var result = []
            for (i = 0; i < iLength; i++) {

                if(JSON.parse(event[i].response_data)[eventType].id===eventId) {
                    result.push(event[i]);
                    //console.log(JSON.parse(event[i].response_data)[eventType].id);
                }
            }
            res.send(result);
        }).catch(err => {

        return res.status(404).send({
            message: "Error retrieving event with id " + eventId +"/"+ eventType
        });
    });

}

//***************************************************************************
// 3. provides via server sent events (SSE) any new change events limited to the ids mentioned above
/*
There are five history of changes are available
taking the 5th objectId and trying to check whether there is any new change made to that event
 */
exports.monitorObjectById = (req, res) => {
    Event.findById(req.params.Id)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "monitorObjectById not found with id " + req.params.Id
                });
            }
            const eventType = event.event_type;
            const eventId = JSON.parse(event.response_data)[eventType].id;
            const lastUpdateDate = JSON.parse(event.response_data)[eventType].updated_at;
            searchRecentEventById(eventId, eventType, lastUpdateDate, res)

        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "monitorObjectById not found with id " + req.params.Id
            });
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.Id
        });
    });
};
function searchRecentEventById(eventId, eventType, lastUpdateDate, res)
{
    const query = {event_type: eventType};
    Event.find(query)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "1 Error retrieving event with id " + eventId +"/"+ eventType
                });
            }
            var i;
            var iLength = event.length;
            var result = [];

            var dt1=new Date(lastUpdateDate);
            //console.log("selected date:", dt1);
            for (i = 0; i < iLength; i++) {

                var dt2=new Date(JSON.parse(event[i].response_data)[eventType].updated_at);
                //console.log("event date:", dt2);
                //console.log(dt2>dt1);

                if((JSON.parse(event[i].response_data)[eventType].id==eventId) && (dt2>dt1)) {
                    result.push(event[i]);
                    //console.log(JSON.parse(event[i].response_data)[eventType].id);
                }
            }
            res.send(result);
        }).catch(err => {
        return res.status(404).send({
            message: "2 Error retrieving event with id " + eventId +"/"+ eventType
        });
    });
}
//***************************************************************************
// 4. provides all new issue updates, including new issues, deleted issues, for the identified repository)
exports.monitorIssues = (req, res) => {
   // Event.find({ event_type: "issues" })
    var query={ $or: [ { event_type: 'issues' }, { event_type: 'issue_comment' } ] };
    Event.find(query)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "monitorIssues not found with id " + req.params.repoId
                });
            }

            var i;
            var iLength = event.length;
            var result = []
            for (i = 0; i < iLength; i++) {

                if(JSON.parse(event[i].response_data).repository.id==req.params.repoId) {
                    result.push(event[i]);
                    //console.log(JSON.parse(event[i].response_data).repository.id);
                }
            }

            res.send(result);

        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "monitorIssues not found with id " + req.params.repoId
            });
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.repoId
        });
    });
};
//***************************************************************************

// 5. provide events on all new commits,  (i.e., new commits and commenting thereof)
exports.monitorCommits = (req, res) => {
    Event.find({ event_type: "commit_comment" })
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "monitorCommits not found with id " + req.params.repoId
                });
            }
            var i;
            var iLength = event.length;
            var result = []
            for (i = 0; i < iLength; i++) {

                if(JSON.parse(event[i].response_data).repository.id==req.params.repoId) {
                    result.push(event[i]);
                    //console.log(JSON.parse(event[i].response_data).repository.id);
                }
            }

            res.send(result);

        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "monitorCommits not found with id " + req.params.repoId
            });
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.repoId
        });
    });
};
//***************************************************************************

// 6. provide events on all new pull requests  (i.e., new pull requests and changes thereof)
exports.monitorPullRequests = (req, res) => {
    //console.log(req.params.repoId);
    var query={event_type:'pull_request'};
    Event.find(query)
        .then(event => {
            if(!event) {
                return res.status(404).send({
                    message: "monitorPullRequests not found with id " + req.params.repoId
                });
            }

            var i;
            var iLength = event.length;
            var result = []
            for (i = 0; i < iLength; i++) {

                if(JSON.parse(event[i].response_data).repository.id==req.params.repoId) {
                    result.push(event[i]);
                   // //console.log(JSON.parse(event[i].response_data).repository.id);
                }
            }

            res.send(result);
        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "monitorPullRequests not found with id " + req.params.repoId
            });
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.repoId
        });
    });
};
//***************************************************************************

// 7. a simple pass through of all events received via the webhook using SSE.
exports.monitorAll = (req, res) => {
    Event.find()
        .then(events => {
            res.send(events);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    });
};

//***************************************************************************

// 6. provide events on all new push
exports.monitorPush = (req, res) => {
    //console.log(req.params.repoId);
    var query={event_type:'push'};
    Event.find(query)
        .then(event => {

            if(!event) {
                return res.status(404).send({
                    message: "monitorPush not found with id " + req.params.repoId
                });
            }

            let i;
            const iLength = event.length;
            const result = [];
            for (i = 0; i < iLength; i++) {

                if(JSON.parse(event[i].response_data).repository.id==req.params.repoId) {
                    result.push(event[i]);
                   //console.log(JSON.parse(event[i].response_data).repository.id==req.params.repoId);
                }
            }
            res.send(result);
        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "monitorPush not found with id " + req.params.repoId
            });
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.repoId
        });
    });
};

// 6. provide events on all new push
exports.monitorPushWithStatus = (req, res) => {
    //console.log(req.params.repoId);
    var query={event_type:'push', active:true};
    Event.find(query)
        .then(event => {

            if(!event) {
                return res.status(404).send({
                    message: "monitorPush not found with id " + req.params.repoId
                });
            }

            let i;
            const iLength = event.length;
            const result = [];
            for (i = 0; i < iLength; i++) {

                if(JSON.parse(event[i].response_data).repository.id==req.params.repoId) {
                    result.push(event[i]);
                   //console.log(JSON.parse(event[i].response_data).repository.id==req.params.repoId);
                }
            }
            res.send(result);
        }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "monitorPush not found with id " + req.params.repoId
            });
        }
        return res.status(500).send({
            message: "Error retrieving event with id " + req.params.repoId
        });
    });
};
//***************************************************************************
//get specific issue details directly from GitHub
exports.getIssuefromGitHub = (req, res) => {
    //console.log(req);
    octokit.issues.get({
        owner:req.params.owner,
        repo:req.params.repo,
        issue_number:req.params.issue_number,
    }).then(({ data }) => {
            //console.log(data);
            res.send(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    });
};

//***************************************************************************

exports.getRepositoryInfo = async (req, res) => {

    const octokit = new Octokit({
        auth: "52270fa9afb54978b114e75a50e7e41d9174a407"
    });

    const {data} = await octokit.request("/user");

    octokit.repos.get({
        owner: repositorInfo.DEFAULT_OWNER,
        repo: repositorInfo.DEFAULT_REPOSITORY
    }).then(({data}) => {
        ////console.log(data);
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    });
};

//***************************************************************************
exports.createWebHook = async (req, res) => {

    const octokit = new Octokit({
        auth: req.body.auth
    });

    const {data} = await octokit.request("/user");

   // //console.log(data+"//"+req.body.owner + "-" + req.body.repo + "\n" + JSON.stringify(req.body.config));

    octokit.repos.createHook({
        owner: req.body.owner,
        repo: req.body.repo,
        config: req.body.config,
        name: req.body.name,
        active: req.body.active,
        events: req.body.events
    }).then(({data}) => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    });
};

function makeInActive(eventId) {
        // Find event and update it with the request body
        Event.findByIdAndUpdate(eventId, {
            active: false
        }, {new: true})
            .then(event => {
                console.log("Status Updated for id: "+eventId);
            }).catch(err => {
            console.log("Status Updated for id: "+ err+"-"+ eventId);
        });
}

//***************************************************************************
exports.addCommitComment = async (req, res) => {
    const octokit = new Octokit({
        auth: req.body.auth
    });
    var mongoId= req.body.mongoId;
    const {data} = await octokit.request("/user");

    octokit.repos.createCommitComment({
        owner: req.body.owner,
        repo: req.body.repo,
        commit_sha: req.body.commit_sha,
        body: req.body.body
    }).then(({data}) => {
        makeInActive(mongoId);
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    });
};
//***************************************************************************

exports.getAssignedIssues = async (req, res) => {

    const octokit = new Octokit({
        auth: "52270fa9afb54978b114e75a50e7e41d9174a407"
    });

    const {data} = await octokit.request("/user");

    octokit.issues.list().then(({data}) => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving events."
        });
    });
/*
    let myFirstPromise = new Promise((resolve, reject) => {
        const octokit = new Octokit({
            auth: "52270fa9afb54978b114e75a50e7e41d9174a407"
        });
        const {data} = octokit.request("/user");
    }).then((successMessage) => {
        octokit.issues.list().then(({data}) => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving events."
            });
        });
    });*/

};
//***************************************************************************
