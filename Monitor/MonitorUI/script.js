var url='http://localhost:5000/';
//===================================================================
activeCheck();
function activeCheck()
{
    const Http = new XMLHttpRequest();
    Http.open("GET", url);
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.status == 200 && Http.readyState==4) {
            document.getElementById('monitor_status').innerHTML = "<b style='color: green'>Active</b>";
        }else {
            document.getElementById('monitor_status').innerHTML = "<b style='color: red'>Inactive</b>";
        }
    }
}
//===================================================================
function loadRepositoryInfo()
{
    const Http = new XMLHttpRequest();
    Http.open("GET", url+'getRepositoryInfo/', true);
    Http.setRequestHeader("contentType","application/json");
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.status == 200 && Http.readyState==4) {
            let response = Http.responseText.toString();
        const data = JSON.parse(response);
        document.getElementById('owner').innerHTML = data.owner.login;
        document.getElementById('repository').innerHTML = data.name;
        document.getElementById('repositoryurl').innerHTML =data.owner.html_url;
        document.getElementById('reponumber').innerHTML = data.id;
        }
    }
}
//===================================================================
function tableFromJson() {
    // the json data. (you can change the values for output.)
    const Http = new XMLHttpRequest();
    Http.open("GET", url+'monitorAll/', true);
    Http.setRequestHeader("contentType","application/json");
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.status == 200 && Http.readyState==4) {
            let response = Http.responseText;
            const data = JSON.parse(response);
            document.getElementById('showData').innerHTML = data.length;
            /*
            for (var k = 0; k < data.length; k++) {
            var myBooks = JSON.parse(data[k]);

            // Extract value from table header.
            var col = [];
            for (var i = 0; i < myBooks.length; i++) {
                for (var key in myBooks[i]) {
                    if (col.indexOf(key) === -1) {
                        col.push(key);
                    }
                }
            }

            // Create a table.
            var table = document.createElement("table");

            // Create table header row using the extracted headers above.
            var tr = table.insertRow(-1);                   // table row.

            for (var i = 0; i < col.length; i++) {
                var th = document.createElement("th");      // table header.
                th.innerHTML = col[i];
                tr.appendChild(th);
            }

            // add json data to the table as rows.
            for (var i = 0; i < myBooks.length; i++) {

                tr = table.insertRow(-1);

                for (var j = 0; j < col.length; j++) {
                    var tabCell = tr.insertCell(-1);
                    tabCell.innerHTML = myBooks[i][col[j]];
                }
            }

            // Now, add the newly created table with json data, to a container.
            var divShowData = document.getElementById('showData');
            divShowData.innerHTML = "";
            divShowData.appendChild(table);

        }}*/
    }}

}
//===================================================================
function monitorIssues()
{
// the json data. (you can change the values for output.)
    const Http = new XMLHttpRequest();
    Http.open("GET", url+'monitorIssues/262132990', true);
    Http.setRequestHeader("contentType","application/json");
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.status == 200 && Http.readyState==4) {

            let response = Http.responseText;
            var data = JSON.parse(response);
            document.getElementById('showmonitorAllIssues').innerHTML = data.length;
        }
    }
}
//===================================================================
function monitorPush()
{
// the json data. (you can change the values for output.)
    const Http = new XMLHttpRequest();
    Http.open("GET", url+'monitorPush/262132990', true);
    Http.setRequestHeader("contentType","application/json");
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.status == 200 && Http.readyState==4) {

            let response = Http.responseText;
            var data = JSON.parse(response);
            document.getElementById('showmonitorPush').innerHTML = data.length;
        }
    }
}
//===================================================================
function monitorPullRequests()
{

    // the json data. (you can change the values for output.)
    const Http = new XMLHttpRequest();
    Http.open("GET", url+'monitorPullRequests/262132990', true);
    Http.setRequestHeader("contentType","application/json");
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.status == 200 && Http.readyState==4) {

            let response = Http.responseText;
            var data = JSON.parse(response);
            document.getElementById('showmonitorPullRequests').innerHTML = data.length;
        }
    }
}
//===================================================================
function monitorCommits()
{

    // the json data. (you can change the values for output.)
    const Http = new XMLHttpRequest();
    Http.open("GET", url+'monitorCommits/262132990', true);
    Http.setRequestHeader("contentType","application/json");
    Http.send();
    Http.onreadystatechange = (e) => {
        if (Http.status == 200 && Http.readyState==4) {

            let response = Http.responseText;
            var data = JSON.parse(response);
            document.getElementById('showmonitorCommits').innerHTML = data.length;
        }
    }
}
//===================================================================
