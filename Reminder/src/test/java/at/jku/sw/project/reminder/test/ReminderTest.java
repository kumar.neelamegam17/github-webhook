package at.jku.sw.project.reminder.test;

import at.jku.sw.project.reminder.*;
import at.jku.sw.project.reminder.domainObjects.ResponseData;
import at.jku.sw.project.reminder.generateddomain.Commit;
import at.jku.sw.project.reminder.generateddomain.Repository;
import at.jku.sw.project.reminder.networkcalls.MonitorServiceGenerator;
import at.jku.sw.project.reminder.networkcalls.ServiceList;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReminderTest {
    public static void main(String[] args) throws IOException {

        ReminderTest reminderTest =new ReminderTest();
        ReminderRemoteCalls reminderRemoteCalls =new ReminderRemoteCalls();
        ReminderRules reminderRules=new ReminderRules();
        ServiceList serviceList = MonitorServiceGenerator.createService(ServiceList.class);

       /* //Monitor Operations
        //startRemoteHookURL
        reminder.startRemoteHookURL(serviceList);
        //stopRemoteHookURL
        reminder.stopRemoteHookURL(serviceList);

        //Cached hook payloads
        reminder.getObjectById(serviceList, "37c7e100-95ca-11ea-83e2-a1592671dedf");
        reminder.getObjectHistoryById(serviceList, "37c7e100-95ca-11ea-83e2-a1592671dedf");
        reminder.monitorObjectById(serviceList, "37c7e100-95ca-11ea-83e2-a1592671dedf");
        reminder.monitorIssues(serviceList, "262132990");
        reminder.monitorCommits(serviceList, "262132990");
        reminder.monitorPullRequests(serviceList, "262132990");
        reminder.monitorAll(serviceList);

        //GitHub Operations
        reminder.getIssuefromGitHub(serviceList, "Kumar-Neelamegam","Experiment","9");

        WebHookConfig webHookConfig=new WebHookConfig();
        webHookConfig.setAuth("52270fa9afb54978b114e75a50e7e41d9174a407");
        webHookConfig.setOwner("Kumar-Neelamegam");
        webHookConfig.setRepo("Experiment");
        webHookConfig.setName("web");
        webHookConfig.setActive(true);
        JsonArray jsonArray=new JsonArray();
                jsonArray.add("push");
                jsonArray.add("pull_request");
                jsonArray.add("issues");
                jsonArray.add("issue_comment");
                jsonArray.add("commit_comment");
        webHookConfig.setEvents(jsonArray);
        JsonObject jsonObject=new JsonObject();
                jsonObject.addProperty("url", "https://example.com/webhook12");
                jsonObject.addProperty("content_type", "json");
                jsonObject.addProperty("insecure_ssl", "0");
        webHookConfig.setConfig(jsonObject);
        reminder.createWebHook(serviceList, webHookConfig);
*/
        Response<JsonArray> jsonArrayResponse=reminderRemoteCalls.getAssignedIssues(serviceList);
        System.out.println(jsonArrayResponse);


    }
    //================================================================
}
