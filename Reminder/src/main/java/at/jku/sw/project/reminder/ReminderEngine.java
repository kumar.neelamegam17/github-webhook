package at.jku.sw.project.reminder;

import at.jku.sw.project.reminder.domainObjects.ResponseData;
import at.jku.sw.project.reminder.generateddomain.Commit;
import at.jku.sw.project.reminder.generateddomain.Repository;
import at.jku.sw.project.reminder.networkcalls.MonitorServiceGenerator;
import at.jku.sw.project.reminder.networkcalls.ServiceList;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import retrofit2.Response;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.*;
import java.util.stream.Stream;

import static at.jku.sw.project.reminder.Utils.getCurrentBeforeDate;

public class ReminderEngine extends JFrame {
    //======================================================================
    public JPanel panelMain;
    public JButton startButton;
    public JButton stopButton;
    public JTextArea engineLog;
    private JScrollPane scrollPane;
    boolean runflag;
    //======================================================================

    /**
     * Main Program
     *
     * @param args
     */
    public static void main(String[] args) {
        ReminderEngine reminderEngine = new ReminderEngine();

        JFrame jFrame = new JFrame("Reminder Engine");
        jFrame.setContentPane(reminderEngine.panelMain);
        jFrame.setSize(1500, 500);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        reminderEngine.initComponents();
        jFrame.pack();
        jFrame.setVisible(true);

    }
    //======================================================================

    /**
     * Initcomponents
     */
    private void initComponents() {
        startButton.setEnabled(true);
        stopButton.setEnabled(false);
        scrollPane.getVerticalScrollBar().addAdjustmentListener(e -> e.getAdjustable().setValue(e.getAdjustable().getMaximum()));
    }
    //======================================================================

    /**
     * Controllisteners
     */
    public ReminderEngine() {
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                engineLog.append("Engine Started..\n\n");
                stopButton.setEnabled(true);
                startButton.setEnabled(false);
                startThread();
            }
        });
        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                engineLog.append("\n\nEngine Stopped..");
                stopButton.setEnabled(false);
                startButton.setEnabled(true);
                stopThread();
            }
        });
    }
    //======================================================================
    /**
     * Thread start and stop the reminder web service
     */
    SwingWorker swingWorker;

    private void startThread() {
        swingWorker = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                try {
                    runflag = true;
                    waitMethod();
                } catch (Exception ex) {
                }
                return 0;
            }
        };
        swingWorker.execute();
    }

    //======================================================================
    private void stopThread() {
        if (swingWorker != null) {
            runflag = false;
            swingWorker.cancel(true);
        }
    }

    //======================================================================
    private synchronized void waitMethod() {
        while (runflag) {
            //engineLog.append("hello"+Calendar.getInstance().getTime()+"\n");
            //System.out.println("always running program ==> " + Calendar.getInstance().getTime());
            try {
                this.wait(5000);//3 second
                //call the reminder web service here and pass the textarea
                checkCommits();
            } catch (Exception e) {
               // e.printStackTrace();
            }
        }
    }
    //======================================================================

    /**
     * Get the issues from the cache by calling monitor api call
     * loop all the latest issues
     * check for the RULE1 - COMMIT CHECK
     *
     * @throws IOException
     */
    static int CurrentListCount;
    private void checkCommits() throws IOException {
        ReminderRemoteCalls reminderRemoteCalls = new ReminderRemoteCalls();
        ReminderRules reminderRules = new ReminderRules();
        ServiceList serviceList = MonitorServiceGenerator.createService(ServiceList.class);
        List allCommitIds = new ArrayList();
        printTextConsole("RULE 1 : Commit checks");
        Response<List<ResponseData>> response = reminderRemoteCalls.monitorPushWithStatus(serviceList, "262132990");
        try {
            CurrentListCount=response.body().size();
            ObjectMapper mapper;
            Repository repository = new Repository();
            List<Commit> commitList = new ArrayList<>();
            List tempCommitIds = new ArrayList();
            for (ResponseData responseData : response.body()) {
                Date createdDate = Utils.getFormattedScheduleEndDate(responseData.getDate());
                Date currentDate = Utils.getCurrentDate();
               if (createdDate.equals(currentDate) || createdDate.after(currentDate)) {
                    //Mapping json objects to java object class
                    JsonObject jsonObject = JsonParser.parseString(responseData.getResponse_data()).getAsJsonObject();
                    mapper = new ObjectMapper();
                    String repository_str = jsonObject.getAsJsonObject("repository").toString();
                    repository = mapper.readValue(repository_str, new TypeReference<Repository>() {
                    });

                    String commits_str = jsonObject.getAsJsonArray("commits").toString();
                    commitList = (mapper.readValue(commits_str, new TypeReference<List<Commit>>() {
                    }));

                    tempCommitIds = new ArrayList();
                    tempCommitIds = reminderRules.commitChecker_RULE1(reminderRemoteCalls, commitList, repository, serviceList, responseData.get_id());
                    allCommitIds.add(tempCommitIds);
                }
            }

            // Convert List to stream
            //Stream<String> stream = Utils.convertListToStream(allCommitIds);
            if(allCommitIds.size()>0)
            {
                for (Object allCommitId : allCommitIds) {
                    if(allCommitId!=null && allCommitId.toString().length()>0)
                    printTextConsole("Commit Id: Updated --> "+allCommitId.toString()+"\n");
                }
            }else
            {
                printTextConsole("RULE 1 COMPLETED PROCESSING"+"\n");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //======================================================================
    public void printTextConsole(String msg) {
        engineLog.append(Calendar.getInstance().getTime() + ": " + msg + "\n");
    }
    //======================================================================

}
