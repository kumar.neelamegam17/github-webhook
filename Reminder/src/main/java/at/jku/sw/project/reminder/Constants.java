package at.jku.sw.project.reminder;

public class Constants {
    //================================================================
    public static String generateClassesPath="./src/main/java/";
    public static String generateClassesPackage="at.jku.sw.project.reminder.generateddomain";
    public static String MonitorUrl = "http://localhost:5000/";
    public static String gitAuthentication = "52270fa9afb54978b114e75a50e7e41d9174a407";
    public static String gitCommitComment_Template="Reminder: Add the issue id in the commit message @";
    //================================================================
}
