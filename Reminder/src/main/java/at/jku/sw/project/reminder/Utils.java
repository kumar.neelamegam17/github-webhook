package at.jku.sw.project.reminder;

import at.jku.sw.project.reminder.domainObjects.ResponseData;
import com.google.gson.JsonObject;
import com.sun.codemodel.JCodeModel;
import org.apache.commons.lang.time.DateUtils;
import org.jsonschema2pojo.*;
import org.jsonschema2pojo.rules.RuleFactory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Utils {
    //================================================================
    private static ReminderRemoteCalls reminderRemoteCalls;
    //================================================================
    public static boolean checkForIssueNumberInCommit(String commitMessage) {
        String regex = ".*#\\d.*";
        return Pattern.matches(regex, commitMessage);
    }
    //================================================================
    public static Date getFormattedScheduleEndDate(String scheduleEnd) throws ParseException {
        return getPushEventDateFormat().parse(scheduleEnd);
    }
    //================================================================
    public static Date getCurrentDate() throws ParseException {
        Date date = new Date();
        String formattedDate = getPushEventDateFormat().format(date);
        return getPushEventDateFormat().parse(formattedDate);
    }

    public static Date getCurrentBeforeDate() throws ParseException {
        int addMinuteTime = 1;
        Date date = new Date();
        String formattedDate = getPushEventDateFormat().format(DateUtils.addMinutes(date, addMinuteTime));
        return getPushEventDateFormat().parse(formattedDate);

    }
    //================================================================
    protected static SimpleDateFormat getPushEventDateFormat() {
        return new SimpleDateFormat("yyyy-MM-dd");
    }
    //================================================================
    public void convert2JSON(String inputJson, File outputPojoDirectory, String packageName, String className) {
        try {
            JCodeModel codeModel = new JCodeModel();
            GenerationConfig config = new DefaultGenerationConfig() {
                @Override
                public boolean isGenerateBuilders() { // set config option by overriding method
                    return true;
                }

                public SourceType getSourceType() {
                    return SourceType.JSON;
                }
            };
            SchemaMapper mapper = new SchemaMapper(new RuleFactory(config, new Jackson2Annotator(config), new SchemaStore()), new SchemaGenerator());
            mapper.generate(codeModel, className, packageName, inputJson);
            codeModel.build(outputPojoDirectory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //================================================================
    public static void createCallBackString(Call<String> responseDataCall) {
        responseDataCall.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                System.out.println(response.body());
            }

            @Override
            public void onFailure(Call<String> call, Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        });
    }
    //================================================================
    public static void createCallBackJsonArrayList(Call<List<ResponseData>> responseDataCall) {
        responseDataCall.enqueue(new Callback<List<ResponseData>>() {
            @Override
            public void onResponse(Call<List<ResponseData>> call, Response<List<ResponseData>> response) {
                System.out.println(response.body());
            }

            @Override
            public void onFailure(Call<List<ResponseData>> call, Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        });
    }
    //================================================================
    public static void createCallBackObject(Call<JsonObject> responseDataCall) {
        responseDataCall.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                System.out.println(response.body());
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        });
    }
    //================================================================
    // Generic function to convert a list to stream
    public static <T> Stream<T> convertListToStream(List<T> list)
    {
        return list.stream();
    }
    //================================================================

}
