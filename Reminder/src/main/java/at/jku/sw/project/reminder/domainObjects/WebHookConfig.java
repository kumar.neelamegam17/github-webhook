package at.jku.sw.project.reminder.domainObjects;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class WebHookConfig {
   /* {
        "auth":"52270fa9afb54978b114e75a50e7e41d9174a407",
            "owner":"Kumar-Neelamegam",
            "repo":"Experiment",
            "name": "web",
            "active": true,
            "events": [
        "push",
                "pull_request",
                "issues",
                "issue_comment",
                "commit_comment"
  ],
        "config": {
        "url": "https://example.com/webhook1",
                "content_type": "json",
                "insecure_ssl": "0"
    }
    }*/

    String auth;
    String owner;
    String repo;
    String name;
    boolean active;
    JsonArray events;
    JsonObject config;

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRepo() {
        return repo;
    }

    public void setRepo(String repo) {
        this.repo = repo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public JsonArray getEvents() {
        return events;
    }

    public void setEvents(JsonArray events) {
        this.events = events;
    }

    public JsonObject getConfig() {
        return config;
    }

    public void setConfig(JsonObject config) {
        this.config = config;
    }
}
