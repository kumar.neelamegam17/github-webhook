
package at.jku.sw.project.reminder.generateddomain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ref",
    "before",
    "after",
    "repository",
    "pusher",
    "sender",
    "created",
    "deleted",
    "forced",
    "base_ref",
    "compare",
    "commits",
    "head_commit"
})
public class ResponseGenerated {

    @JsonProperty("ref")
    private String ref;
    @JsonProperty("before")
    private String before;
    @JsonProperty("after")
    private String after;
    @JsonProperty("repository")
    private Repository repository;
    @JsonProperty("pusher")
    private Pusher pusher;
    @JsonProperty("sender")
    private Sender sender;
    @JsonProperty("created")
    private Boolean created;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("forced")
    private Boolean forced;
    @JsonProperty("base_ref")
    private Object baseRef;
    @JsonProperty("compare")
    private String compare;
    @JsonProperty("commits")
    private List<Commit> commits = new ArrayList<Commit>();
    @JsonProperty("head_commit")
    private HeadCommit headCommit;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ref")
    public String getRef() {
        return ref;
    }

    @JsonProperty("ref")
    public void setRef(String ref) {
        this.ref = ref;
    }

    public ResponseGenerated withRef(String ref) {
        this.ref = ref;
        return this;
    }

    @JsonProperty("before")
    public String getBefore() {
        return before;
    }

    @JsonProperty("before")
    public void setBefore(String before) {
        this.before = before;
    }

    public ResponseGenerated withBefore(String before) {
        this.before = before;
        return this;
    }

    @JsonProperty("after")
    public String getAfter() {
        return after;
    }

    @JsonProperty("after")
    public void setAfter(String after) {
        this.after = after;
    }

    public ResponseGenerated withAfter(String after) {
        this.after = after;
        return this;
    }

    @JsonProperty("repository")
    public Repository getRepository() {
        return repository;
    }

    @JsonProperty("repository")
    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public ResponseGenerated withRepository(Repository repository) {
        this.repository = repository;
        return this;
    }

    @JsonProperty("pusher")
    public Pusher getPusher() {
        return pusher;
    }

    @JsonProperty("pusher")
    public void setPusher(Pusher pusher) {
        this.pusher = pusher;
    }

    public ResponseGenerated withPusher(Pusher pusher) {
        this.pusher = pusher;
        return this;
    }

    @JsonProperty("sender")
    public Sender getSender() {
        return sender;
    }

    @JsonProperty("sender")
    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public ResponseGenerated withSender(Sender sender) {
        this.sender = sender;
        return this;
    }

    @JsonProperty("created")
    public Boolean getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(Boolean created) {
        this.created = created;
    }

    public ResponseGenerated withCreated(Boolean created) {
        this.created = created;
        return this;
    }

    @JsonProperty("deleted")
    public Boolean getDeleted() {
        return deleted;
    }

    @JsonProperty("deleted")
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ResponseGenerated withDeleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    @JsonProperty("forced")
    public Boolean getForced() {
        return forced;
    }

    @JsonProperty("forced")
    public void setForced(Boolean forced) {
        this.forced = forced;
    }

    public ResponseGenerated withForced(Boolean forced) {
        this.forced = forced;
        return this;
    }

    @JsonProperty("base_ref")
    public Object getBaseRef() {
        return baseRef;
    }

    @JsonProperty("base_ref")
    public void setBaseRef(Object baseRef) {
        this.baseRef = baseRef;
    }

    public ResponseGenerated withBaseRef(Object baseRef) {
        this.baseRef = baseRef;
        return this;
    }

    @JsonProperty("compare")
    public String getCompare() {
        return compare;
    }

    @JsonProperty("compare")
    public void setCompare(String compare) {
        this.compare = compare;
    }

    public ResponseGenerated withCompare(String compare) {
        this.compare = compare;
        return this;
    }

    @JsonProperty("commits")
    public List<Commit> getCommits() {
        return commits;
    }

    @JsonProperty("commits")
    public void setCommits(List<Commit> commits) {
        this.commits = commits;
    }

    public ResponseGenerated withCommits(List<Commit> commits) {
        this.commits = commits;
        return this;
    }

    @JsonProperty("head_commit")
    public HeadCommit getHeadCommit() {
        return headCommit;
    }

    @JsonProperty("head_commit")
    public void setHeadCommit(HeadCommit headCommit) {
        this.headCommit = headCommit;
    }

    public ResponseGenerated withHeadCommit(HeadCommit headCommit) {
        this.headCommit = headCommit;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public ResponseGenerated withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ResponseGenerated.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("ref");
        sb.append('=');
        sb.append(((this.ref == null)?"<null>":this.ref));
        sb.append(',');
        sb.append("before");
        sb.append('=');
        sb.append(((this.before == null)?"<null>":this.before));
        sb.append(',');
        sb.append("after");
        sb.append('=');
        sb.append(((this.after == null)?"<null>":this.after));
        sb.append(',');
        sb.append("repository");
        sb.append('=');
        sb.append(((this.repository == null)?"<null>":this.repository));
        sb.append(',');
        sb.append("pusher");
        sb.append('=');
        sb.append(((this.pusher == null)?"<null>":this.pusher));
        sb.append(',');
        sb.append("sender");
        sb.append('=');
        sb.append(((this.sender == null)?"<null>":this.sender));
        sb.append(',');
        sb.append("created");
        sb.append('=');
        sb.append(((this.created == null)?"<null>":this.created));
        sb.append(',');
        sb.append("deleted");
        sb.append('=');
        sb.append(((this.deleted == null)?"<null>":this.deleted));
        sb.append(',');
        sb.append("forced");
        sb.append('=');
        sb.append(((this.forced == null)?"<null>":this.forced));
        sb.append(',');
        sb.append("baseRef");
        sb.append('=');
        sb.append(((this.baseRef == null)?"<null>":this.baseRef));
        sb.append(',');
        sb.append("compare");
        sb.append('=');
        sb.append(((this.compare == null)?"<null>":this.compare));
        sb.append(',');
        sb.append("commits");
        sb.append('=');
        sb.append(((this.commits == null)?"<null>":this.commits));
        sb.append(',');
        sb.append("headCommit");
        sb.append('=');
        sb.append(((this.headCommit == null)?"<null>":this.headCommit));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.pusher == null)? 0 :this.pusher.hashCode()));
        result = ((result* 31)+((this.compare == null)? 0 :this.compare.hashCode()));
        result = ((result* 31)+((this.before == null)? 0 :this.before.hashCode()));
        result = ((result* 31)+((this.created == null)? 0 :this.created.hashCode()));
        result = ((result* 31)+((this.forced == null)? 0 :this.forced.hashCode()));
        result = ((result* 31)+((this.repository == null)? 0 :this.repository.hashCode()));
        result = ((result* 31)+((this.headCommit == null)? 0 :this.headCommit.hashCode()));
        result = ((result* 31)+((this.baseRef == null)? 0 :this.baseRef.hashCode()));
        result = ((result* 31)+((this.ref == null)? 0 :this.ref.hashCode()));
        result = ((result* 31)+((this.deleted == null)? 0 :this.deleted.hashCode()));
        result = ((result* 31)+((this.sender == null)? 0 :this.sender.hashCode()));
        result = ((result* 31)+((this.commits == null)? 0 :this.commits.hashCode()));
        result = ((result* 31)+((this.after == null)? 0 :this.after.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ResponseGenerated) == false) {
            return false;
        }
        ResponseGenerated rhs = ((ResponseGenerated) other);
        return (((((((((((((((this.pusher == rhs.pusher)||((this.pusher!= null)&&this.pusher.equals(rhs.pusher)))&&((this.compare == rhs.compare)||((this.compare!= null)&&this.compare.equals(rhs.compare))))&&((this.before == rhs.before)||((this.before!= null)&&this.before.equals(rhs.before))))&&((this.created == rhs.created)||((this.created!= null)&&this.created.equals(rhs.created))))&&((this.forced == rhs.forced)||((this.forced!= null)&&this.forced.equals(rhs.forced))))&&((this.repository == rhs.repository)||((this.repository!= null)&&this.repository.equals(rhs.repository))))&&((this.headCommit == rhs.headCommit)||((this.headCommit!= null)&&this.headCommit.equals(rhs.headCommit))))&&((this.baseRef == rhs.baseRef)||((this.baseRef!= null)&&this.baseRef.equals(rhs.baseRef))))&&((this.ref == rhs.ref)||((this.ref!= null)&&this.ref.equals(rhs.ref))))&&((this.deleted == rhs.deleted)||((this.deleted!= null)&&this.deleted.equals(rhs.deleted))))&&((this.sender == rhs.sender)||((this.sender!= null)&&this.sender.equals(rhs.sender))))&&((this.commits == rhs.commits)||((this.commits!= null)&&this.commits.equals(rhs.commits))))&&((this.after == rhs.after)||((this.after!= null)&&this.after.equals(rhs.after))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))));
    }

}
