package at.jku.sw.project.reminder.networkcalls;

import at.jku.sw.project.reminder.domainObjects.ResponseData;
import at.jku.sw.project.reminder.domainObjects.WebHookConfig;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

import java.util.List;

public interface ServiceList {

    @GET("/startRemoteHookURL/")
    Call<JsonObject> startRemoteHookURL();

    @GET("/stopRemoteHookURL/")
    Call<JsonObject> stopRemoteHookURL();

    @GET("/getObjectById/{Id}")
    Call<ResponseData> getObjectById(@Path("Id") String Id);

    @GET("/getObjectHistoryById/{Id}")
    Call<List<ResponseData>> getObjectHistoryById(@Path("Id") String Id);

    @GET("/monitorObjectById/{Id}")
    Call<List<ResponseData>> monitorObjectById(@Path("Id") String Id);

    @GET("/monitorIssues/{repoId}")
    Call<List<ResponseData>> monitorIssues(@Path("repoId") String repoId);

    @GET("/monitorCommits/{repoId}")
    Call<List<ResponseData>> monitorCommits(@Path("repoId") String repoId);

    @GET("/monitorPush/{repoId}")
    Call<List<ResponseData>> monitorPush(@Path("repoId") String repoId);

    @GET("/monitorPushWithStatus/{repoId}")
    Call<List<ResponseData>> monitorPushWithStatus(@Path("repoId") String repoId);

    @GET("/monitorPullRequests/{repoId}")
    Call<List<ResponseData>> monitorPullRequests(@Path("repoId") String repoId);

    @GET("/monitorAll")
    Call<List<ResponseData>> monitorAll();

    @GET("/getIssuefromGitHub/{owner}/{repo}/{issue_number}")
    Call<JsonObject> getIssuefromGitHub(@Path("owner") String owner, @Path("repo") String repo, @Path("issue_number") String issue_number);

    @POST("/createWebHook/")
    Call<JsonObject> createWebHook(@Body WebHookConfig config);

    @POST("/addCommitComment/")
    Call<JsonObject> addCommitComment(@Body JsonObject config);

    @GET("/getAssignedIssues/")
    Call<JsonArray> getAssignedIssues();

}
