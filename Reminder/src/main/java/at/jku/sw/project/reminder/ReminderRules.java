package at.jku.sw.project.reminder;

import at.jku.sw.project.reminder.generateddomain.AssignedIssue;
import at.jku.sw.project.reminder.generateddomain.Commit;
import at.jku.sw.project.reminder.generateddomain.Repository;
import at.jku.sw.project.reminder.networkcalls.ServiceList;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static at.jku.sw.project.reminder.Constants.gitAuthentication;
import static at.jku.sw.project.reminder.Constants.gitCommitComment_Template;

public class ReminderRules {

    //================================================================
    /**
     * RULE 1
     * MONITOR THE PUSH COMMITS AND CHECK FOR ISSUE ID
     * IF NOT MAKE A REMINDER TO THE USER
     * MAKE A COMMIT COMMENT
     * @param logic
     * @param commitList
     * @param repository
     * @param serviceList
     * @throws IOException
     */
    public List<String> commitChecker_RULE1(ReminderRemoteCalls logic, List<Commit> commitList, Repository repository, ServiceList serviceList, String mongoId) throws IOException {
        try {
            List list=new ArrayList();
            for (Commit commit : commitList) {


                if(Utils.checkForIssueNumberInCommit(commit.getMessage()))//check commit messages whether they have issue number
                {
                    continue;
                }else {
                    System.out.println("commit = " + commit.getId() + ", message = " + commit.getMessage());
                    String comment=gitCommitComment_Template+ commit.getAuthor().getUsername();
                    JsonObject jsonObject=new JsonObject();
                    jsonObject.addProperty("auth", gitAuthentication);
                    jsonObject.addProperty("owner", repository.getOwner().getName());
                    jsonObject.addProperty("repo", repository.getName());
                    jsonObject.addProperty("body", comment);
                    jsonObject.addProperty("commit_sha", commit.getId());
                    jsonObject.addProperty("mongoId", mongoId);
                    logic.addCommitComment(serviceList, jsonObject);
                    list.add(commit.getId());
                }

            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //================================================================
    /**
     * RULE 2
     */
    //================================================================
    public List<String> getAssignedIssues_RULE2(ReminderRemoteCalls logic, List<AssignedIssue> assignedIssues, Repository repository, ServiceList serviceList) throws IOException {
        try {
            List list=new ArrayList();
            for (AssignedIssue assignedIssue : assignedIssues) {
                System.out.println("AssignedIssue = " + assignedIssue.getId());

            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    //================================================================


}
