package at.jku.sw.project.reminder;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

    public static SimpleDateFormat DATE_FORMAT;

    public static Date convertStringToDate(final String str, String format){
        try{
            DATE_FORMAT = new SimpleDateFormat(format);
            return DATE_FORMAT.parse(str);
        } catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public static String convertDateToString(final Date date, String format){
        try{
            DATE_FORMAT = new SimpleDateFormat(format);
            return DATE_FORMAT.format(date);
        } catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    }
}
