package at.jku.sw.project.reminder;

import at.jku.sw.project.reminder.domainObjects.ResponseData;
import at.jku.sw.project.reminder.domainObjects.WebHookConfig;
import at.jku.sw.project.reminder.networkcalls.ServiceList;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static at.jku.sw.project.reminder.Constants.*;

public class ReminderRemoteCalls {

    //================================================================
    public ReminderRemoteCalls() throws IOException {
    }
    //================================================================
    public Response<JsonObject> startRemoteHookURL(ServiceList serviceList) throws IOException {
        Call<JsonObject> responseDataCall = serviceList.startRemoteHookURL();
        Response<JsonObject> response = responseDataCall.execute();
        return response;
    }
    //================================================================
    public Response<JsonObject> stopRemoteHookURL(ServiceList serviceList) throws IOException {
        Call<JsonObject> responseDataCall = serviceList.stopRemoteHookURL();
        Response<JsonObject> response = responseDataCall.execute();
        return response;
    }
    //================================================================
    public Response<JsonObject> createWebHook(ServiceList serviceList, WebHookConfig webHookConfig) throws IOException {
        Call<JsonObject> responseDataCall = serviceList.createWebHook(webHookConfig);
        Response<JsonObject> response = responseDataCall.execute();
        return response;
    }
    //================================================================
    public void getObjectById(ServiceList serviceList, String Id) throws IOException {
        final Call<ResponseData> responseDataCall = serviceList.getObjectById(Id);
        responseDataCall.enqueue(new Callback<ResponseData>() {
            @Override
            public void onResponse(Call<ResponseData> call, Response<ResponseData> response) {
                System.out.println(response.body().getResponse_data());
                String responseData = response.body().getResponse_data();
                JsonObject jsonObject = JsonParser.parseString(responseData).getAsJsonObject();
                Utils utils = new Utils();
                utils.convert2JSON(jsonObject.toString(), new File(generateClassesPath), generateClassesPackage, "responseGenerated");
            }

            @Override
            public void onFailure(Call<ResponseData> call, Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        });
    }
    //================================================================
    public void getObjectHistoryById(ServiceList serviceList, String Id) throws IOException {
        Call<List<ResponseData>> responseDataCall = serviceList.getObjectHistoryById(Id);
        Utils.createCallBackJsonArrayList(responseDataCall);
    }
    //================================================================
    public void monitorObjectById(ServiceList serviceList, String Id) {
        Call<List<ResponseData>> responseDataCall = serviceList.monitorObjectById(Id);
        Utils.createCallBackJsonArrayList(responseDataCall);
    }
    //================================================================
    public Response<List<ResponseData>> monitorIssues(ServiceList serviceList, String repositoryId) throws IOException {
        Call<List<ResponseData>> responseDataCall = serviceList.monitorIssues(repositoryId);
        Response<List<ResponseData>> response = responseDataCall.execute();
        return response;
    }
    //================================================================
    public Response<List<ResponseData>> monitorCommits(ServiceList serviceList, String repositoryId) throws IOException {
        Call<List<ResponseData>> responseDataCall = serviceList.monitorCommits(repositoryId);
        Response<List<ResponseData>> response = responseDataCall.execute();
        return response;
    }
    //================================================================
    public Response<List<ResponseData>> monitorPullRequests(ServiceList serviceList, String repositoryId) throws IOException {
        Call<List<ResponseData>> responseDataCall = serviceList.monitorPullRequests(repositoryId);
        Response<List<ResponseData>> responseData = responseDataCall.execute();
        return responseData;
    }
    //================================================================
    public Response<List<ResponseData>> monitorAll(ServiceList serviceList) throws IOException {
        Call<List<ResponseData>> responseDataCall = serviceList.monitorAll();
        Response<List<ResponseData>> responseData = responseDataCall.execute();
        callJson2PojoConvertor(responseData.body().get(0).getResponse_data(),"monitorAll");
        return responseData;
    }
    //================================================================
    public Response<JsonObject> getIssuefromGitHub(ServiceList serviceList, String owner, String repo, String issue_number) throws IOException {
        Call<JsonObject> responseDataCall = serviceList.getIssuefromGitHub(owner, repo, issue_number);
        Response<JsonObject> response = responseDataCall.execute();
        return response;
    }
    //================================================================
    public Response<List<ResponseData>> monitorPushWithStatus(final ServiceList serviceList, String repositoryId) throws IOException {
        final Call<List<ResponseData>> responseDataCall = serviceList.monitorPushWithStatus(repositoryId);
        Response<List<ResponseData>> response = responseDataCall.execute();
        return response;
    }
    //================================================================
    public Response<JsonObject> addCommitComment(ServiceList serviceList, JsonObject content) throws IOException {
        Call<JsonObject> responseDataCall = serviceList.addCommitComment(content);
        Response<JsonObject> response = responseDataCall.execute();
        return response;
    }
    //================================================================
    public Response<JsonArray> getAssignedIssues(ServiceList serviceList) throws IOException {
        Call<JsonArray> responseDataCall = serviceList.getAssignedIssues();
        Response<JsonArray> response = responseDataCall.execute();
        //callJson2PojoConvertor(response.body().toString(), "AssignedIssues");
        return response;
    }
    //================================================================
    public void callJson2PojoConvertor(String responseJson, String className)
    {
      Utils utils = new Utils();
      utils.convert2JSON(responseJson, new File(generateClassesPath), generateClassesPackage, className);
    }
    //================================================================

}
