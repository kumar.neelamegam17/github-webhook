package at.jku.sw.project.reminder.networkcalls;

import com.google.gson.stream.JsonReader;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static at.jku.sw.project.reminder.Constants.MonitorUrl;

public class MonitorServiceGenerator {
    private static final String BASE_URL = MonitorUrl;

    private static Retrofit.Builder builder
            = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create());

    private static Retrofit retrofit = builder.build();

    private static OkHttpClient.Builder httpClient= new OkHttpClient.Builder();

    public static <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
